const express = require("express");
const app = express();

const port = 3000;

let users = [];

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.get("/", (req, res) => {
	res.send("Hello World");
});

app.get("/home",(req, res) => {
	res.send("welcome to the home page");
});

app.get("/users",(req, res) => {
    res.send(users)
})

app.post("/", (req, res) => {
    res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

app.post("/signup", (req, res) => {
	if(req.body.username !== "" && req.body.password !== "") {
		users.push(req.body);
		res.send(`User ${req.body.username} is successfully registered!`);
	} else {
		res.send(`Please input BOTH username and password!`);
	}
	console.log(req.body);
});

app.put("/change-password", (req, res) => {
	let message;
	for(let user = 0; user < users.length; user++) {
		if(req.body.username == users[user].username) {
			users[user].password = req.body.password;
			message = `User ${req.body.username} has succesfully change their password`;
			break;
		} else {
			message = `User ${req.body.username} does not exist`;
		}
	}
	res.send(message);
	console.log(users);
})

app.delete("/delete-user", (req, res) => {
    let message2;
    for(let i = 0; i < users.length; i++){
        if(req.body.username === users[i].username) {
            users.pop(req.body)
            message2 = `user ${req.body.username} has been deleted.`
        }
    }
    res.send(message2);
    console.log(users);
})


app.listen(port, () => console.log(`Server is listening at localhost:${port}`));